# F-Droid rc3 World

This is Work-Adventure map for the F-Droid assembly on the rc3 online-confernce.

This is based on https://git.cccv.de/rc3/world-map-starterkit so traces of the sample project might remain.


This map can be tested here: https://play.meet.n2n.io/_/global/uniqx.gitlab.io/fdroid-rc3-world/main.json#start


This project also uses some 3rd party tilesets:
* https://v-ktor.itch.io/32x32-rpg-tilesets CC-4.0-BY Viktor Hahn
* https://pipoya.itch.io/pipoya-rpg-tileset-32x32
